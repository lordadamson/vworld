extends Node

const DEFAULT_PORT = 8080

var players = Array()

func player_erase(id):
	for p in players:
		if(p.id == id):
			players.erase(p)

func player_position_update(id, position):
	for p in players:
		if(p.id == id):
			p.position = position

func _player_connected(id):
	print("Player ", id, " connected! :D")
	var position = Vector2(randi() % 320, randi() % 320 )
	
	var player = {
		"id": id,
		"name": get_node("random_name").get_random_name(),
		"position": position
	}
	
	players.append(player)
	
	rpc("_player_entered_client", player)
	rpc_id(id, "_post_connection_client", players)

func _player_disconnected(id):
	print("Player ", id, " disconnected :(")
	player_erase(id)
	rpc("_player_left_client", id)

@rpc("any_peer")
func _on_position_changed_server(player_id, new_position):
	player_position_update(player_id, new_position)
	rpc("_on_player_position_update_client", player_id, new_position)

@rpc("any_peer")
func _on_message_server(player_id, message):
	print(player_id, " says: ", message)
	
	if(message == "/hubby" or message == "\n/hubby"):
		rpc("_on_emote_client", player_id, "hubby")
		return
	elif(message == "/sad" or message == "\n/sad"):
		rpc("_on_emote_client", player_id, "sad")
		return
	elif(message == "/angry" or message == "\n/angry"):
		rpc("_on_emote_client", player_id, "angry")
		return
	elif(message == "/devious" or message == "\n/devious"):
		rpc("_on_emote_client", player_id, "devious")
		return
	elif(message == "/happy" or message == "\n/happy"):
		rpc("_on_emote_client", player_id, "happy")
		return
	elif(message == "/giggle" or message == "\n/giggle"):
		rpc("_on_emote_client", player_id, "giggle")
		return
	elif(message == "/love" or message == "\n/love"):
		rpc("_on_emote_client", player_id, "love")
		return
	elif(message == "/wow" or message == "\n/wow"):
		rpc("_on_emote_client", player_id, "wow")
		return
	elif(message == "/wat" or message == "\n/wat"):
		rpc("_on_emote_client", player_id, "wat")
		return
	
	rpc("_on_message_client", player_id, message)

@rpc
func _player_entered_client(p_player):
	pass

@rpc
func _on_player_position_update_client(player_id, new_position):
	pass

@rpc
func _player_left_client(id):
	pass

@rpc
func _on_message_client(player_id, message):
	pass

@rpc
func _on_emote_client(player_id, animation):
	pass

@rpc
func _post_connection_client(p_players):
	pass

func _ready():
	var host = ENetMultiplayerPeer.new()
	var err = host.create_server(DEFAULT_PORT)
	if(err):
		print("Can't start server :(")
	else:
		print("Listening on 0.0.0.0:", DEFAULT_PORT)
	
	multiplayer.multiplayer_peer = host
	
	multiplayer.connect("peer_connected", Callable(self, "_player_connected"))
	multiplayer.connect("peer_disconnected", Callable(self, "_player_disconnected"))
