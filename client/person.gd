extends Node2D

func _on_message_bubble_timer():
	get_node("message_bubble").queue_free()

func display_message(message):
	if(has_node("message_bubble")):
		var message_bubble = get_node("message_bubble")
		message_bubble.get_node("message_label").text = message
		message_bubble.get_node("timer").start()
		return
	
	var message_bubble = load("res://message_bubble.tscn").instantiate()
	message_bubble.get_node("message_label").text = message
	message_bubble.position.y -= 100
	message_bubble.position.x += 50
	message_bubble.get_node("timer").connect("timeout", Callable(self, "_on_message_bubble_timer"))
	add_child(message_bubble)

func _on_emote_timeout():
	get_node("animated_sprite").stop()
	get_node("animated_sprite").visible = false

func play_animation(animation):
	get_node("animated_sprite").visible = true
	get_node("animated_sprite").play(animation)
	get_node("emote_timer").start()

func _ready():
	get_node("emote_timer").connect("timeout", Callable(self, "_on_emote_timeout"))
