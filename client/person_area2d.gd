extends Area2D

signal position_changed

var dragging = false

func _on_input_event(viewport, event, shape_idx):
	if not event is InputEventMouseButton:
		return
	
	if event.pressed:
		dragging = true
	else:
		dragging = false

func _ready():
	connect("input_event", Callable(self, "_on_input_event"))

func _process(delta):
	if not dragging:
		return
	
	get_parent().position = get_viewport().get_mouse_position() - Vector2(0, 60)
	emit_signal("position_changed", get_parent().position)