extends Node

const REMOTE = "127.0.0.1"
const DEFAULT_PORT = 8080

var my_id = 0
var connected = false
var players = Array()

static func delete_children(node):
	for n in node.get_children():
		node.remove_child(n)
		n.queue_free()

func player_position_update(id, position):
	for p in players:
		if(p.id == id):
			p.position = position

func _connected_ok():
	my_id = multiplayer.get_unique_id()
	print("Connected successfully")

func _connected_fail():
	print("Connection failed")


func _server_disconnected():
	print("Server disconnected")

func _on_position_changed(new_position):
	rpc_id(1, "_on_position_changed_server", my_id, new_position)

func get_player(id):
	var ps = get_node("/root/root/players/")
	return ps.get_node(str(id))

@rpc
func _player_entered_client(p_player):
	players.append(p_player)
	
	var player = load("res://person.tscn").instantiate()

	player.position = p_player.position
	player.get_node("name").text = str(p_player.name)
	
	player.name = str(p_player.id)
	
	if(p_player.id != my_id):
		player.get_node("sprite_area").queue_free()
	else:
		player.get_node("name").text += " (you)"
	
	get_node("players").add_child(player)

@rpc
func _on_player_position_update_client(player_id, new_position):
	player_position_update(player_id, new_position)
	get_player(player_id).position = new_position

@rpc
func _player_left_client(id):
	id = id[0].id
	for p in players:
		if(p.id == id):
			players.erase(p)
	
	var ps = get_node("/root/root/players/")
	var p = ps.get_node(str(id))
	ps.remove_child(p)
	p.queue_free()

@rpc
func _on_message_client(player_id, message):
	get_player(player_id).display_message(message)

@rpc
func _on_emote_client(player_id, animation):
	print(animation)
	get_player(player_id).play_animation(animation)

@rpc
func _post_connection_client(p_players):
	players.clear()
	delete_children(get_node("players"))
	
	for p in p_players:
		_player_entered_client(p)
	
	connected = true
	print("Connected successfully")
	var p = get_player(my_id)
	p.get_node("sprite_area").connect("position_changed", Callable(self, "_on_position_changed"))

func _input(event):
	if not (event is InputEventKey and event.pressed and event.keycode == KEY_ENTER):
		return
	
	var message = get_node("message_te").text
	get_node("message_te").text = ""
	
	rpc_id(1, "_on_message_server", my_id, message)

@rpc("any_peer")
func _on_position_changed_server(player_id, new_position):
	pass

@rpc("any_peer")
func _on_message_server(player_id, message):
	pass
	
func _ready():
	var host = ENetMultiplayerPeer.new()
	host.create_client(REMOTE, DEFAULT_PORT)
	multiplayer.multiplayer_peer = host
	
	print("Connecting...")
	
	multiplayer.connect("connected_to_server", Callable(self, "_connected_ok"))
	multiplayer.connect("connection_failed", Callable(self, "_connected_fail"))
	multiplayer.connect("server_disconnected", Callable(self, "_server_disconnected"))
